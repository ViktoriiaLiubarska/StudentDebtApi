﻿using System;
using System.Collections.Generic;
using System.Text;

namespace apiStudentDebt.Models
{
    class Debt
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public double Amount { get; set; }
        public double MonthlyPercent { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Debt debt &&
                   Id == debt.Id &&
                   StudentId == debt.StudentId &&
                   Amount == debt.Amount &&
                   MonthlyPercent == debt.MonthlyPercent;
        }

        public bool EqualsWithoutID(object obj)
        {
            if (obj is Debt debt)
            {
                debt.Id = 0;
                return Equals(obj);
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, StudentId, Amount, MonthlyPercent);
        }

        public override string ToString()
        {
            return "Id: " + Id + " StudentId: " + StudentId + " Amount: " + Amount
                + " MonthlyPercent: " + MonthlyPercent;
        }
    }
}
