﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace apiStudentDebt.Models
{
    class Collector
    {
        public int Id { get; set; }
        public string Nick_Name { get; set; }
        public int FearFactor { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Collector collector &&
                   Id == collector.Id &&
                   Nick_Name == collector.Nick_Name &&
                   FearFactor == collector.FearFactor;
        }

        public bool EqualsWithoutID(object obj)
        {
            if (obj is Collector collector)
            {
                collector.Id = 0;
                return Equals(obj);
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Nick_Name, FearFactor);
        }

        public override string ToString()
        {
            return "Id: " + Id + " NickName: " + Nick_Name + " FearFactor: " + FearFactor;
        }
    }
}
