﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace apiStudentDebt.Models
{
    class Appointment
    {
        public int Id { get; set; }
        public int DebtId { get; set; }
        public int[] CollectorIds { get; set; }
        public DateTime AppointmentDate { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Appointment appointment &&
                   Id == appointment.Id &&
                   DebtId == appointment.DebtId &&
                   CollectorIds.SequenceEqual(appointment.CollectorIds) &&
                   AppointmentDate == appointment.AppointmentDate;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, DebtId, CollectorIds, AppointmentDate);
        }

        public override string ToString()
        {
            string collectorIds = " ";
            foreach (var id in CollectorIds)
            {
                collectorIds += id + " ";
            }
            return "Id: " + Id + " DebtId: " + DebtId + " CollectorIds: [" + collectorIds + "]"
                + " AppointmentDate: " + AppointmentDate;
        }
    }
}
