﻿using System;
using System.Collections.Generic;
using System.Text;

namespace apiStudentDebt.Models
{
    class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Int64 Age { get; set; }
        public bool Sex { get; set; }
        public int Risk { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Student student &&
                   Id == student.Id &&
                   Name == student.Name &&
                   Age == student.Age &&
                   Sex == student.Sex &&
                   Risk == student.Risk;
        }

        public bool EqualsWithoutID(object obj)
        {
            if (obj is Student student)
            {
                student.Id = 0;
                return Equals(obj);
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, Age, Sex, Risk);
        }

        public override string ToString()
        {
            return "Id: " + Id + " Name: " + Name + " Age: " + Age + " Sex: " + Sex
                + " Risk: " + Risk;
        }

    }
}
