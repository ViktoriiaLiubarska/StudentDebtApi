﻿using apiStudentDebt.Models;
using System;

namespace apiStudentDebt.Factories
{
    class TestDataStudentFactory
    {
        public static Student CreateTestStudent(int id = 0, Int64 age = 5500000000000000,
            int risk = 2)

        {
            Student student = new Student();
            student.Id = id;
            student.Name = "Anastasiia Rimakova";
            student.Age = age;
            student.Sex = false;
            student.Risk = risk;
            return student;
        }

        public static Student GetUpdatedTestStudent(int id, Int64 age = 6500000000000000,
            int risk = 5)

        {
            Student student = new Student();
            student.Id = id;
            student.Name = "Anastasiia Rimakova";
            student.Age = age;
            student.Sex = false;
            student.Risk = risk;
            return student;
        }

    }
}
