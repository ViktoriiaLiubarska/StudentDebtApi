﻿using apiStudentDebt.Models;


namespace apiStudentDebt.Factories
{
    class TestDataCollectorFactory
    {
        public static Collector CreateTestCollector(int id = 0, int fearFactor = 5)
        {
            Collector collector = new Collector();
            collector.Id = id;
            collector.Nick_Name = "Vladimir Liubarsky";
            collector.FearFactor = fearFactor;
            return collector;
        }

        public static Collector GetUpdatedTestCollector(int id, int fearFactor = 7)
        {
            Collector collector = new Collector();
            collector.Id = id;
            collector.Nick_Name = "Vladimir Liubarsky";
            collector.FearFactor = fearFactor;
            return collector;
        }

    }
}
