﻿using apiStudentDebt.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace apiStudentDebt.Factories
{
    class TestDataDebtFactory
    {
        public static Debt CreateTestDebt(int id = 0, int studentId = 2, double amount = 600,
            double percent = 1.5)
        {
            Debt debt = new Debt();
            debt.Id = id;
            debt.StudentId = studentId;
            debt.Amount = amount;
            debt.MonthlyPercent = percent;
            return debt;
        }
    }
}
