﻿using apiStudentDebt.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace apiStudentDebt.Factories
{
    class TestDataAppointmentFactory
    {
        public static Appointment CreateTestAppointment(int debtId = 0)
        {
            Appointment appointment = new Appointment();
            appointment.DebtId = debtId;
            return appointment;
        }
    }
}
