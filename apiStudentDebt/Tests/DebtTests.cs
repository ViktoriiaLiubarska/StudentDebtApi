﻿using apiStudentDebt.Factories;
using apiStudentDebt.Models;
using apiStudentDebt.Api;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace apiStudentDebt
{
    [TestClass]
    public class DebtTests
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Debt debtRequestModel = TestDataDebtFactory.CreateTestDebt();
        private Debt debtResponseModel = null;
        private readonly DebtApi debtsApi = new DebtApi();

        [TestInitialize()]
        public void Initialize()
        {
            debtResponseModel = debtsApi.PostRequest(debtRequestModel, out _);
        }

        [TestCleanup()]
        public void Cleanup()
        {
            debtsApi.DeleteRequest(debtResponseModel.Id, out _);
        }


        [DataTestMethod]
        [DataRow(1.5)]
        [DataRow(2.3)]
        [DataRow(3.4)]
        [DataRow(0.9)]
        [DataRow(4.6)]
        [DataRow(1.7)]
        [DataRow(6.8)]
        public void CreateNewDebt(double percent)
        {
            var debtRequestModel = TestDataDebtFactory.CreateTestDebt(percent: percent);
            var responseDebt = debtsApi.PostRequest(debtRequestModel, out int statusCode);
            Logger.Info("POST Request: " + debtRequestModel);
            Logger.Info("POST Response: " + responseDebt);
            debtsApi.DeleteRequest(responseDebt.Id, out _);
            Assert.AreEqual(201, statusCode, "Invalid status code");
            Assert.IsTrue(debtRequestModel.EqualsWithoutID(responseDebt), "POST request and response without ID are not equal");
        }


        [TestMethod]
        public void ViewAllDebts()
        {
            var responseDebts = debtsApi.GetRequest(out int statusCode);
            Logger.Info("GET Response: ");
            foreach (var debt in responseDebts)
            {
                Logger.Info(debt);
            }
            Assert.AreEqual(200, statusCode, "Invalid status code");
            Assert.IsTrue(responseDebts.Length > 0, "List of debts is empty");
        }

        [TestMethod]
        public void ViewDebt()
        {
            var responseDebt = debtsApi.GetRequestWithParam(debtResponseModel.Id, out int statusCode);
            Logger.Info("POST Response: " + debtResponseModel);
            Logger.Info("GET Response: " + responseDebt);
            Assert.AreEqual(200, statusCode, "Invalid status code");
            Assert.AreEqual(debtResponseModel, responseDebt, "POST response and GET response are not equal");
        }

        [TestMethod]
        public void DeleteDebt()
        {
            debtsApi.DeleteRequest(debtResponseModel.Id, out int statusCode);
            Assert.AreEqual(200, statusCode, "Invalid DELETE status code");
            debtsApi.GetRequestWithParam(debtResponseModel.Id, out int statusCodeGET);
            Assert.AreEqual(404, statusCodeGET, "Invalid GET status code");
        }

        [TestMethod]
        public void VerifyThatFieldAmountOfNewlyCreatedDebtIsNotChangeAfterGET()
        {
            var responseDebt1 = debtsApi.GetRequestWithParam(debtResponseModel.Id, out _);
            var responseDebt2 = debtsApi.GetRequestWithParam(debtResponseModel.Id, out _);
            Logger.Info("GET Response: " + responseDebt1);
            Logger.Info("GET Response: " + responseDebt2);
            Assert.AreEqual(responseDebt2.Amount, responseDebt1.Amount, "Field amount has different values");
        }

        [TestMethod]
        public void VerifyThatFieldAmountOfPreviouslyCreatedDebtIsNotChangeAfterGET()
        {
            var responseDebt1 = debtsApi.GetRequestWithParam(0, out _);
            var responseDebt2 = debtsApi.GetRequestWithParam(0, out _);
            Logger.Info("GET Response: " + responseDebt1);
            Logger.Info("GET Response: " + responseDebt2);
            Assert.AreEqual(responseDebt2.Amount, responseDebt1.Amount, "Field amount has different values");
        }

        [TestMethod]
        public void VerifyImpossibilityToCreateDebtForNonexistentStudent()
        {
            int maxStudentId = new StudentApi().GetAllIds().Max();
            var debtRequestModel = TestDataDebtFactory.CreateTestDebt(studentId: maxStudentId + 1);
            var responseDebt = debtsApi.PostRequest(debtRequestModel, out int statusCode);
            Logger.Info("POST Request: " + debtRequestModel);
            if (responseDebt != null)
            {
                Logger.Info("POST Response: " + responseDebt);
                debtsApi.DeleteRequest(responseDebt.Id, out _);
            }
            Assert.AreEqual(400, statusCode, "Invalid status code when debt was created for non-existent student");
        }

        [TestMethod]
        public void VerifyImpossibilityToDeleteDebtIfAppointmentCreatedforThisDebtCollection()
        {
            var appointmentRequestModel = TestDataAppointmentFactory.CreateTestAppointment(debtResponseModel.Id);
            var responseAppointment = new AppointmentApi().PostRequest(appointmentRequestModel, out _);
            Logger.Info("Appointment POST response: " + responseAppointment);
            debtsApi.DeleteRequest(debtResponseModel.Id, out int statusCode);
            new AppointmentApi().DeleteRequest(responseAppointment.Id, out _);
            Assert.AreEqual(400, statusCode, "Invalid status code, no validation for linked appointment and debt");
        }


        [DataTestMethod]
        [DataRow(0)]
        [DataRow(-300)]
        public void VerifyValidationOfFieldAmountDuringDebtCreation(double amount)
        {
            var debtRequestModel = TestDataDebtFactory.CreateTestDebt(amount: amount);
            var responseDebt = debtsApi.PostRequest(debtRequestModel, out int statusCode);
            Logger.Info("POST Request: " + debtRequestModel);
            if (responseDebt != null)
            { 
                debtsApi.DeleteRequest(responseDebt.Id, out _); 
            }
            Assert.AreEqual(400, statusCode, "Invalid status code, there is no validation of field Amount");
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(-2)]
        public void VerifyValidationOfFieldMonthlyPercentDuringDebtCreation(double percent)
        {
            var debtRequestModel = TestDataDebtFactory.CreateTestDebt(percent: percent);
            var responseDebt = debtsApi.PostRequest(debtRequestModel, out int statusCode);
            Logger.Info("POST Request: " + debtRequestModel);
            if (responseDebt != null)
            {
                debtsApi.DeleteRequest(responseDebt.Id, out _);
            }
            Assert.AreEqual(400, statusCode, "Invalid status code, there is no validation of field MonthlyPercent");
        }

        [TestMethod]
        public void VerifyThatRecordFor16thAndFurtherDebtsWithCorrectIdIsCreated()
        {
            Debt responseDebt = null;
            int maxId = debtsApi.GetAllIds().Max();
            Logger.Debug("maxId: " + maxId);
            if (maxId < 14)
            {
                Logger.Debug("Validation for 16th debt");
                for (int i = maxId; i < 15; i++)
                {
                    responseDebt = debtsApi.PostRequest(debtRequestModel, out _);
                }
                Logger.Info("POST Response: " + responseDebt);
                for (int i = maxId; i < 15; i++)
                {
                    debtsApi.DeleteRequest(i + 1, out _);
                }
                debtsApi.DeleteRequest(-1, out _);
                Assert.AreEqual(15, responseDebt.Id, "Incorrect id is created for 15th record");
            }
            else
            {
                responseDebt = debtsApi.PostRequest(debtRequestModel, out _);
                int id = debtsApi.GetRequest(out _).Length - 1;
                Logger.Info("POST Request: " + debtRequestModel);
                Logger.Info("POST Response: " + responseDebt);
                debtsApi.DeleteRequest(-1, out int statusCode2);
                Assert.AreEqual(id, responseDebt.Id, $"Incorrect id is created for {id}th record");
            }
        }
    }
}
