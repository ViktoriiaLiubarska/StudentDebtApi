using apiStudentDebt.Factories;
using apiStudentDebt.Models;
using apiStudentDebt.Api;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace apiStudentDebt
{
    [TestClass]
    public class CollectorTests
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Collector collectorRequestModel = TestDataCollectorFactory.CreateTestCollector();
        private Collector collectorResponseModel = null;
        private readonly CollectorApi collectorsApi = new CollectorApi();

        [TestInitialize()]
        public void Initialize()
        {
            collectorResponseModel = collectorsApi.PostRequest(collectorRequestModel, out _);
        }

        [TestCleanup()]
        public void Cleanup()
        {
            collectorsApi.DeleteRequest(collectorResponseModel.Id, out _);
        }


        [TestMethod]
        public void CreateNewCollector()
        {
            var responseCollector = collectorsApi.PostRequest(collectorRequestModel, out int statusCode);
            Logger.Info("POST Request: " + collectorRequestModel);
            Logger.Info("POST Response: " + responseCollector);
            Assert.AreEqual(201, statusCode, "Invalid status code");
            Assert.IsTrue(collectorRequestModel.EqualsWithoutID(responseCollector), "POST request and response without ID are not equal");
        }

        [TestMethod]
        public void EditCollector()
        {
            var requestCollector = TestDataCollectorFactory.GetUpdatedTestCollector(collectorResponseModel.Id);
            var responseCollector = collectorsApi.PutRequest(requestCollector, out int statusCode);
            Logger.Info("PUT Request: " + requestCollector);
            Logger.Info("PUT Response: " + responseCollector);
            Assert.AreEqual(200, statusCode, "Invalid status code");
            Assert.AreEqual(requestCollector, responseCollector, "PUT request and PUT response are not equal");

            var responseCollectortGET = collectorsApi.GetRequestWithParam(collectorResponseModel.Id, out _);
            Logger.Info("GET Response: " + responseCollectortGET);
            Assert.AreEqual(requestCollector, responseCollectortGET, "PUT request and GET response are not equal");
        }

        [TestMethod]
        public void ViewAllCollectors()
        {
            var responseCollectors = collectorsApi.GetRequest(out int statusCode);
            Logger.Info("GET Response: ");
            foreach (var collector in responseCollectors)
            {
                Logger.Info(collector);
            }
            Assert.AreEqual(200, statusCode, "Invalid status code");
            Assert.IsTrue(responseCollectors.Length > 0, "List of collectors is empty");
        }

        [TestMethod]
        public void ViewCollector()
        {
            var responseCollector = collectorsApi.GetRequestWithParam(collectorResponseModel.Id, out int statusCode);
            Logger.Info("POST Response: " + collectorResponseModel);
            Logger.Info("GET Response: " + responseCollector);
            Assert.AreEqual(200, statusCode, "Invalid status code");
            Assert.AreEqual(collectorResponseModel, responseCollector, "POST response and GET response are not equal");
        }

        [TestMethod]
        public void DeleteCollector()
        {
            collectorsApi.DeleteRequest(collectorResponseModel.Id, out int statusCode);
            Assert.AreEqual(200, statusCode, "Invalid DELETE status code");
            collectorsApi.GetRequestWithParam(collectorResponseModel.Id, out int statusCodeGET);
            Assert.AreEqual(404, statusCodeGET, "Invalid GET status code");
        }

        [TestMethod]
        public void VerifyImpossibilityToDeleteCollectorIfHeHasAppointment()
        {
            var appointmentRequestModel = TestDataAppointmentFactory.CreateTestAppointment();
            var responseAppointment = new AppointmentApi().PostRequest(appointmentRequestModel, out _);
            Logger.Info("Appointment POST response: " + responseAppointment);
            collectorsApi.DeleteRequest(responseAppointment.CollectorIds[0], out int statusCode);
            new AppointmentApi().DeleteRequest(responseAppointment.Id, out _);
            Assert.AreEqual(400, statusCode, "Invalid status code, no validation for linked appointment and collector");
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(-5)]
        public void VerifyValidationOfFieldFearFactorDuringCollectorCreation(int factor)
        {
            var requestCollector = TestDataCollectorFactory.CreateTestCollector(fearFactor: factor);
            var responseCollector = collectorsApi.PostRequest(requestCollector, out int statusCode);
            Logger.Info("POST Request: " + requestCollector);
            if (responseCollector != null)
            {
                collectorsApi.DeleteRequest(responseCollector.Id, out _);
            }
            Assert.AreEqual(400, statusCode, "Invalid status code, there is no validation of field Fear factor");
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(-2)]
        public void VerifyValidationOfFieldAgeDuringCollectorUpdate(int factor)
        {
            var requestCollector = TestDataCollectorFactory.GetUpdatedTestCollector(collectorResponseModel.Id, factor);
            var responseCollector = collectorsApi.PutRequest(requestCollector, out int statusCode);
            Logger.Info("PUT Request: " + requestCollector);
            if (responseCollector != null)
            {
                collectorsApi.DeleteRequest(responseCollector.Id, out _);
            }
            Assert.AreEqual(400, statusCode, "Invalid status code, there is no validation of field Fear factor");
        }

        [TestMethod]
        public void CheckThatRecordFor16thAndFurtherCollectorsWithCorrectIdIsCreated()
        {
            Collector responseCollector = null;
            int maxId = collectorsApi.GetAllIds().Max();
            Logger.Debug("maxId: " + maxId);
            if (maxId < 14)
            {
                Logger.Debug("Validation for 16th collector");
                for (int i = maxId; i < 15; i++)
                {
                    responseCollector = collectorsApi.PostRequest(collectorRequestModel, out _);
                }
                Logger.Info("POST Response: " + responseCollector);
                for (int i = maxId; i < 15; i++)
                {
                    collectorsApi.DeleteRequest(i + 1, out _);
                }
                collectorsApi.DeleteRequest(-1, out _);
                Assert.AreEqual(15, responseCollector.Id, "Incorrect id is created for 15th record");
            }
            else
            {
                responseCollector = collectorsApi.PostRequest(collectorRequestModel, out _);
                int id = collectorsApi.GetRequest(out _).Length;
                Logger.Info("POST Request: " + collectorRequestModel);
                Logger.Info("POST Response: " + responseCollector);
                collectorsApi.DeleteRequest(-1, out _);
                Assert.AreEqual(id, responseCollector.Id, $"Incorrect id is created for {id}th record");
            }
        }
    }
}
