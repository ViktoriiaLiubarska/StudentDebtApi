using apiStudentDebt.Factories;
using apiStudentDebt.Models;
using apiStudentDebt.Api;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System;

namespace apiStudentDebt
{
    [TestClass]
    public class StudentTests
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Student studentRequestModel = TestDataStudentFactory.CreateTestStudent();
        private Student studentResponseModel = null;
        private readonly StudentApi studentsApi = new StudentApi();


        [TestInitialize()]
        public void Initialize()
        {
            studentResponseModel = studentsApi.PostRequest(studentRequestModel, out _);
        }

        [TestCleanup()]
        public void Cleanup()
        {
            studentsApi.DeleteRequest(studentResponseModel.Id, out _);
        }


        [TestMethod]
        public void CreateNewStudent()
        {
            var responseStudent = studentsApi.PostRequest(studentRequestModel, out int statusCode);
            Logger.Info("POST Request: " + studentRequestModel);
            Logger.Info("POST Response: " + responseStudent);
            Assert.AreEqual(201, statusCode, "Invalid status code");
            Assert.IsTrue(studentRequestModel.EqualsWithoutID(responseStudent), "POST request and response without ID are not equal");
        }

        [TestMethod]
        public void EditStudent()
        {
            var requestStudent = TestDataStudentFactory.GetUpdatedTestStudent(studentResponseModel.Id);
            var responseStudent = studentsApi.PutRequest(requestStudent, out int statusCode);
            Logger.Info("PUT Request: " + requestStudent);
            Logger.Info("PUT Response: " + responseStudent);
            Assert.AreEqual(200, statusCode, "Invalid status code");
            Assert.AreEqual(requestStudent, responseStudent, "PUT request and PUT response are not equal");

            var responseStudentGET = studentsApi.GetRequestWithParam(studentResponseModel.Id, out _);
            Logger.Info("GET Response: " + responseStudentGET);
            Assert.AreEqual(requestStudent, responseStudentGET, "PUT request and GET response are not equal");
        }

        [TestMethod]
        public void ViewAllStudents()
        {
            var responseStudents = studentsApi.GetRequest(out int statusCode);
            Logger.Info("GET Response: ");
            foreach (var student in responseStudents)
            {
                Logger.Info(student);
            }
            Assert.AreEqual(200, statusCode, "Invalid status code");
            Assert.IsTrue(responseStudents.Length > 0, "List of students is empty");
        }

        [TestMethod]
        public void ViewStudent()
        {
            var responseStudent = studentsApi.GetRequestWithParam(studentResponseModel.Id, out int statusCode);
            Logger.Info("POST Response: " + studentResponseModel);
            Logger.Info("GET Response: " + responseStudent);
            Assert.AreEqual(200, statusCode, "Invalid status code");
            Assert.AreEqual(studentResponseModel, responseStudent, "POST response and GET response are not equal");
        }

        [TestMethod]
        public void DeleteStudent()
        {
            studentsApi.DeleteRequest(studentResponseModel.Id, out int statusCode);
            Assert.AreEqual(200, statusCode, "Invalid DELETE status code");
            studentsApi.GetRequestWithParam(studentResponseModel.Id, out int statusCodeGET);
            Assert.AreEqual(404, statusCodeGET, "Invalid GET status code");
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(-5500000000000000)]
        public void VerifyValidationOfFieldAgeDuringStudentCreation(Int64 age)
        {
            var requestStudent = TestDataStudentFactory.CreateTestStudent(age: age);
            var responseStudent = studentsApi.PostRequest(requestStudent, out int statusCode);
            Logger.Info("POST Request: " + requestStudent);
            if (responseStudent != null)
            {
                studentsApi.DeleteRequest(responseStudent.Id, out _);
            }
            Assert.AreEqual(400, statusCode, "Invalid status code, there is no validation of field age");
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(-6500000000000000)]
        public void VerifyValidationOfFieldAgeDuringStudentUpdate(Int64 age)
        {
            var requestStudent = TestDataStudentFactory.GetUpdatedTestStudent(studentResponseModel.Id, age);
            var responseStudent = studentsApi.PutRequest(requestStudent, out int statusCode);
            Logger.Info("PUT Request: " + requestStudent);
            if (responseStudent != null)
            {
                studentsApi.DeleteRequest(responseStudent.Id, out _);
            }
            Assert.AreEqual(400, statusCode, "Invalid status code, there is no validation of field age");
        }

        [DataTestMethod]
        [DataRow(-2)]
        [DataRow(3)]
        public void VerifyValidationOfFieldRiskDuringStudentCreation(int risk)
        {
            var requestStudent = TestDataStudentFactory.CreateTestStudent(risk: risk);
            var responseStudent = studentsApi.PostRequest(requestStudent, out int statusCode);
            Logger.Info("POST Request: " + requestStudent);
            if (responseStudent != null)
            {
                studentsApi.DeleteRequest(responseStudent.Id, out _);
            }
            Assert.AreEqual(400, statusCode, "Invalid status code, there is no validation of field risk");
        }

        [DataTestMethod]
        [DataRow(-1)]
        [DataRow(6)]
        public void VerifyValidationOfFieldRiskDuringStudentUpdate(int risk)
        {
            var requestStudent = TestDataStudentFactory.GetUpdatedTestStudent(studentResponseModel.Id, risk: risk);
            var responseStudent = studentsApi.PutRequest(requestStudent, out int statusCode);
            Logger.Info("PUT Request: " + requestStudent);
            if (responseStudent != null)
            {
                studentsApi.DeleteRequest(responseStudent.Id, out _);
            }
            Assert.AreEqual(400, statusCode, "Invalid status code, there is no validation of field risk");
        }

        [TestMethod]
        public void VerifyImpossibilityToDeleteStudentIfStudentHasDebt()
        {
            var responseDebt = new DebtApi().PostRequest(TestDataDebtFactory.CreateTestDebt(studentId: studentResponseModel.Id), out _);
            Logger.Info("Debt POST response: " + responseDebt);
            studentsApi.DeleteRequest(studentResponseModel.Id, out int statusCode);
            new DebtApi().DeleteRequest(responseDebt.Id, out _);
            Assert.AreEqual(400, statusCode, "No validation for linked student and his debt");
        }

        [TestMethod]
        public void VerifyThatRecordFor16thAndFurtherStudentsWithCorrectIdIsCreated()
        {
            Student responseStudent = null;
            int maxId = studentsApi.GetAllIds().Max();
            Logger.Debug("maxId: " + maxId);
            if (maxId < 14)
            {
                Logger.Debug("Validation for 16th student");
                for (int i = maxId; i < 15; i++)
                {
                    responseStudent = studentsApi.PostRequest(studentRequestModel, out _);
                }
                Logger.Info("POST Request: " + studentRequestModel);
                Logger.Info("POST Response: " + responseStudent);
                for (int i = maxId; i < 15; i++)
                {
                    studentsApi.DeleteRequest(i + 1, out _);
                }
                studentsApi.DeleteRequest(-1, out _);
                Assert.AreEqual(15, responseStudent.Id, "Incorrect id is created for 15th record");
            }
            else
            {
                responseStudent = studentsApi.PostRequest(studentRequestModel, out _);
                int id = studentsApi.GetRequest(out _).Length;
                Logger.Info("POST Request: " + studentRequestModel);
                Logger.Info("POST Response: " + responseStudent);
                studentsApi.DeleteRequest(-1, out _);
                Assert.AreEqual(id, responseStudent.Id, $"Incorrect id is created for {id}th record");
            }
        }

        [TestMethod]
        public void VerifyThatNewRecordWithCorrectIdCreated()
        {
            studentsApi.DeleteRequest(studentResponseModel.Id, out _);
            int maxId = studentsApi.GetAllIds().Max();
            var responseStudent = studentsApi.PostRequest(studentRequestModel, out _);
            Logger.Info("POST Request: " + studentRequestModel);
            Logger.Info("POST Response: " + responseStudent);
            studentsApi.DeleteRequest(responseStudent.Id, out _);
            Assert.AreEqual(maxId + 1, responseStudent.Id, "Incorrect id is created for new record");
        }
    }
}

