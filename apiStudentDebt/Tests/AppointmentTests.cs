﻿using apiStudentDebt.Factories;
using apiStudentDebt.Models;
using apiStudentDebt.Api;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace apiStudentDebt
{
    [TestClass]
    public class AppointmentTests
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Appointment appointmentRequestModel = TestDataAppointmentFactory.CreateTestAppointment();
        private Appointment appointmentResponseModel = null;
        private readonly AppointmentApi appointmentsApi = new AppointmentApi();
        private readonly DebtApi debtsApi = new DebtApi();
        private readonly StudentApi studentsApi = new StudentApi();
        private readonly CollectorApi collectorsApi = new CollectorApi();


        [TestInitialize()]
        public void Initialize()
        {
            appointmentResponseModel = appointmentsApi.PostRequest(appointmentRequestModel, out _);
        }

        [TestCleanup()]
        public void Cleanup()
        {
            appointmentsApi.DeleteRequest(appointmentResponseModel.Id, out _);
        }


        [TestMethod]
        public void CreateNewAppointment()
        {
            var responseAppointments = appointmentsApi.GetRequest(out _);
            var responseAppointment = appointmentsApi.PostRequest(appointmentRequestModel, out int statusCode);
            var responseDebt = debtsApi.GetRequestWithParam(appointmentRequestModel.Id, out _);
            var responseStudent = studentsApi.GetRequestWithParam(responseDebt.StudentId, out _);

            List<int> collectorFearFactors = GetCollectorFearFactors(responseAppointment);
            var studentRisk = responseStudent.Risk;

            Logger.Debug("Student risk: " + studentRisk);
            Logger.Debug("Total fear factor: " + 2 * collectorFearFactors.Sum());
            Logger.Info("POST Appointment Response: " + responseAppointment);

            Assert.IsTrue(studentRisk < 2 * collectorFearFactors.Sum(), "Student risk is not less than total fear factor");
            Assert.AreEqual(201, statusCode, "Invalid status code");


            List<DateTime> existingAppointmentDates = GetExistingAppointmentDates(responseAppointments, responseAppointment);
            if (existingAppointmentDates.Count == 0)
                Assert.AreEqual(responseAppointment.AppointmentDate.Date, DateTime.Now.Date.AddDays(1), "Appointment date is not correct");
            else
            {
                Trace.WriteLine("existingAppointmentDates.Max(): " + existingAppointmentDates.Max());
                Assert.AreEqual(responseAppointment.AppointmentDate.Date, existingAppointmentDates.Max().AddDays(1), "Appointment date is not correct");
            }
        }

        private List<int> GetCollectorFearFactors(Appointment responseAppointment)
        {
            List<int> collectorFearFactors = new List<int>();
            foreach (var collectorId in responseAppointment.CollectorIds)
            {
                var responseCollector = collectorsApi.GetRequestWithParam(collectorId, out _);
                collectorFearFactors.Add(responseCollector.FearFactor);
            }

            return collectorFearFactors;
        }

        private static List<DateTime> GetExistingAppointmentDates(Appointment[] responseAppointments, Appointment responseAppointment)
        {
            DateTime appointmentDate = responseAppointment.AppointmentDate.Date;
            Logger.Debug("Appointment date: " + appointmentDate);
            List<DateTime> existingAppointmentDates = new List<DateTime>();
            foreach (var appointment in responseAppointments)
            {
                foreach (var collectorId in appointment.CollectorIds)
                {
                    if (responseAppointment.CollectorIds.Contains(collectorId))
                    {
                        existingAppointmentDates.Add(appointment.AppointmentDate.Date);
                    }
                }
            }
            Logger.Debug("Date now: " + DateTime.Now.Date);
            return existingAppointmentDates;
        }

        [TestMethod]
        public void ViewAllAppointments()
        {
            var responseAppointments = appointmentsApi.GetRequest(out int statusCode);
            Logger.Info("GET response: ");
            foreach (var appointment in responseAppointments)
            {
                Logger.Info(appointment);
            }
            Assert.AreEqual(200, statusCode, "Invalid status code");
            Assert.IsTrue(responseAppointments.Length > 0, "List of appointments is empty");
        }

        [TestMethod]
        public void ViewAppointment()
        {
            var responseAppointment = appointmentsApi.GetRequestWithParam(appointmentResponseModel.Id, out int statusCode);
            Logger.Info("POST Response: " + appointmentResponseModel);
            Logger.Info("GET Response: " + responseAppointment);
            Assert.AreEqual(200, statusCode, "Invalid satus code");
            Assert.AreEqual(appointmentResponseModel, responseAppointment, "POST response and GET response are not equal");
        }

        [TestMethod]
        public void DeleteAppointment()
        {
            appointmentsApi.DeleteRequest(appointmentResponseModel.Id, out int statusCode);
            Assert.AreEqual(200, statusCode, "Invalid DELETE status code");
            appointmentsApi.GetRequestWithParam(appointmentResponseModel.Id, out statusCode);
            Assert.AreEqual(204, statusCode, "Invalid GET status code");
        }

        [TestMethod]
        public void VerifyImpossibilityToCreateAppointmentForNonexistentDebt()
        {
            int maxDebtId = new DebtApi().GetAllIds().Max();
            var appointmentRequestModel = TestDataAppointmentFactory.CreateTestAppointment(maxDebtId + 1);
            var responseAppointment = appointmentsApi.PostRequest(appointmentRequestModel, out int statusCode);
            Logger.Debug("DebtId in POST Request: " + (maxDebtId + 1));
            if (responseAppointment != null)
            {
                Logger.Debug("POST Response: " + responseAppointment);
                debtsApi.DeleteRequest(responseAppointment.Id, out _);
            }
            Assert.AreEqual(400, statusCode, "Invalid status code when appointment is created for non-existent debt");
        }


        [TestMethod]
        public void VerifyThatRecordFor16thAndFurtherAppointmentsWithCorrectIdIsCreated()
        {
            Appointment responseAppointment = null;
            int maxId = appointmentsApi.GetAllIds().Max();
            Logger.Debug("maxId: " + maxId);
            if (maxId < 14)
            {
                Logger.Debug("Validation for 16th appointment");
                for (int i = maxId; i < 15; i++)
                {
                    responseAppointment = appointmentsApi.PostRequest(appointmentRequestModel, out _);
                }
                Logger.Info("POST Response: " + responseAppointment);
                for (int i = maxId; i < 15; i++)
                {
                    appointmentsApi.DeleteRequest(i + 1, out _);
                }
                appointmentsApi.DeleteRequest(-1, out _);
                Assert.AreEqual(15, responseAppointment.Id, "Incorrect id is created for 15th record");
            }
            else
            {
                responseAppointment = appointmentsApi.PostRequest(appointmentRequestModel, out _);
                int id = appointmentsApi.GetRequest(out _).Length;
                Logger.Info("POST Request: " + appointmentRequestModel);
                Logger.Info("POST Response: " + responseAppointment);
                appointmentsApi.DeleteRequest(-1, out _);
                Assert.AreEqual(id, responseAppointment.Id, $"Incorrect id is created for {id}th record");
            }
        }
    }
}
