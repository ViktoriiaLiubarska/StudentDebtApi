﻿using apiStudentDebt.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace apiStudentDebt.Api
{
    class CollectorApi : RestApi<Collector>
    {
        private static readonly string collectorPath = "http://localhost:81/api/collector";

        public CollectorApi() : base(collectorPath)
        { }
       
        public List<int> GetAllIds()
        {
            List<int> ids = new List<int>();
            var response = GetRequest(out int statusCode);
            foreach (var item in response)
            {
                ids.Add(item.Id);
            }
            return ids;
        }
    }
}
