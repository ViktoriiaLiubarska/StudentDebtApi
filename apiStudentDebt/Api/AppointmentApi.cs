﻿using apiStudentDebt.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace apiStudentDebt.Api
{
    class AppointmentApi : RestApi<Appointment>
    {
        private static readonly string appointmentPath = "http://localhost:81/api/appointment";

        public AppointmentApi() : base(appointmentPath)
        { }

        public List<int> GetAllIds()
        {
            List<int> ids = new List<int>();
            var response = GetRequest(out int statusCode);
            foreach (var item in response)
            {
                ids.Add(item.Id);
            }
            return ids;
        }

    }
}
