﻿using apiStudentDebt.Models;
using System.Collections.Generic;

namespace apiStudentDebt.Api
{
    class StudentApi : RestApi<Student>
    {
        private static readonly string studentPath = "http://localhost:81/api/student";

        public StudentApi() : base(studentPath)
        { }

        public List<int> GetAllIds()
        {
            List<int> ids = new List<int>();
            var response = GetRequest(out int statusCode);
            foreach (var item in response)
            {
                ids.Add(item.Id);
            }
            return ids;
        }
    }
}
