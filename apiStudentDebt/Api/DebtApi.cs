﻿using apiStudentDebt.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace apiStudentDebt.Api
{
    class DebtApi : RestApi<Debt>
    {
        private static readonly string debtPath = "http://localhost:81/api/debt";

        public DebtApi() : base(debtPath)
        { }

        public List<int> GetAllIds()
        {
            List<int> ids = new List<int>();
            var response = GetRequest(out int statusCode);
            foreach (var item in response)
            {
                ids.Add(item.Id);
            }
            return ids;
        }
    }
}
