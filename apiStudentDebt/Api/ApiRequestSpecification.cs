﻿using System;
using System.Collections.Generic;
using System.Text;
using RestSharp;

namespace apiStudentDebt.Api
{
    class ApiRequestSpecification
    {
        public static RestClient GetRequestSpecification(string path)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri(path);
            client.AddDefaultHeader("Content-Type", "application/json");
            client.AddDefaultHeader("accept", "text/plain");
            return client;
        }
    }
}
