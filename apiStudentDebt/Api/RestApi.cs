﻿using RestSharp;


namespace apiStudentDebt.Api
{
    class RestApi<T>
    {
        public string Path { get; set; }

        public RestApi(string path)
        {
            Path = path;
        }

        public T PostRequest(T obj, out int statusCode)
        {
            var client = ApiRequestSpecification.GetRequestSpecification(Path);
            var request = new RestRequest().AddJsonBody(obj);
            var response = client.Post(request);
            statusCode = (int)response.StatusCode;
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(response.Content);
        }

        public T PutRequest(T obj, out int statusCode)
        {
            var client = ApiRequestSpecification.GetRequestSpecification(Path);
            var request = new RestRequest().AddJsonBody(obj);
            var response = client.Put(request);
            statusCode = (int)response.StatusCode;
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(response.Content);
        }

        public T[] GetRequest(out int statusCode)
        {
            var client = ApiRequestSpecification.GetRequestSpecification(Path);
            var request = new RestRequest();
            var response = client.Get(request);
            statusCode = (int)response.StatusCode;
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T[]>(response.Content);
        }

        public T GetRequestWithParam(int id, out int statusCode)
        {
            var client = ApiRequestSpecification.GetRequestSpecification(Path);
            var request = new RestRequest("{id}").AddUrlSegment("id", id);
            var response = client.Get(request);
            statusCode = (int)response.StatusCode;
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(response.Content);
        }

        public T DeleteRequest(int id, out int statusCode)
        {
            var client = ApiRequestSpecification.GetRequestSpecification(Path);
            var request = new RestRequest("{id}").AddUrlSegment("id", id);
            var response = client.Delete(request);
            statusCode = (int)response.StatusCode;
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}
